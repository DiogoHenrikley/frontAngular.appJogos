/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LigasListaComponent } from './ligas-lista.component';

describe('LigasListaComponent', () => {
  let component: LigasListaComponent;
  let fixture: ComponentFixture<LigasListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LigasListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LigasListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
