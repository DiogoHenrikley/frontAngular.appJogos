import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableColunas } from 'src/app/components/_models/DataTableColunas';
import { DataTableAcoes } from 'src/app/components/_models/DataTableAcoes';
import { LigasModel } from 'src/app/core/models/ligas.model';
import { Router } from '@angular/router';
import { LigasService } from 'src/app/core/services/ligas.service';

@Component({
  selector: 'app-ligas-lista',
  templateUrl: './ligas-lista.component.html',
  styleUrls: ['./ligas-lista.component.css']
})
export class LigasListaComponent implements OnInit {

  colunas: DataTableColunas[] = [
    { propriedade: 'id', titulo: 'Id', disabled: false, cell: (row:  LigasModel) => `${row.id}` },
    { propriedade: 'name', titulo: 'Nome', disabled: false, cell: (row:  LigasModel) => `${row.name}` },
    { propriedade: 'flagActive', titulo: 'Status', disabled: false, cell: (row:  LigasModel) => row.flagActive ? 'Ativo' : 'Inativo' },
  ];

  acoes: DataTableAcoes[] = [
    { icone: 'create', evento: this.editar.bind(this), toolTip: 'Editar', color: 'primary' },
    { icone: 'delete', evento: this.deletar.bind(this), toolTip: 'Deletar', color: 'warn'}
  ];

  dadosTabela: LigasModel[] = [];

  filtroNome: string = '';

  constructor(private route: Router, private ligaService: LigasService) {

  }

  ngOnInit() {

    this.ligaService.obterTodos().subscribe((result: any) => {
      this.dadosTabela = result.lista;
    })
  }

  adicionar(){
    this.route.navigate(['ligas/cadastro/add']);
  }

  editar(liga: LigasModel){
    this.route.navigate([`ligas/cadastro/edit/${liga.id}`]);
  } 

  deletar(){

  }

  buscar(){
    this.ligaService.obterPorDescricao(this.filtroNome).subscribe((result : any) => {
      this.dadosTabela = result.dados
    })
  }
}

