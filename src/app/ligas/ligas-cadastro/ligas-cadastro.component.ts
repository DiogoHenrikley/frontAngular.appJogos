import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LigasModel } from 'src/app/core/models/ligas.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LigasService } from 'src/app/core/services/ligas.service';
import { UploadModel } from 'src/app/core/models/upload.model';
import { enUploadPasta } from 'src/app/core/enumeradores/enuploadPasta';
import { UploadService } from 'src/app/core/services/upload.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-ligas-cadastro',
  templateUrl: './ligas-cadastro.component.html',
  styleUrls: ['./ligas-cadastro.component.css']
})
export class LigasCadastroComponent implements OnInit {
  tituloPagina: string = 'Cadastro de Ligas';
  ligaForm: FormGroup;
  liga: LigasModel;
  ligaOld: LigasModel;
  existeErro: boolean = false;
  file: File;
  arquivoImg: string = '';
  arquivoAdd: string = '';
  base64Image = '';
  nomeImagemPrevisualizacao = '';
  urlImagem = '';


  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private ligaFB: FormBuilder,
              private toast: ToastrService,
              private ligaService: LigasService,
              private uploadService: UploadService) { }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      const id = params.id;
      if (id && id > 0) {
        this.tituloPagina = `Editar Liga - Nº ${id}`;
        this.BuscaPorId(id);
      }
      else{
        const novaLiga = new LigasModel();
        novaLiga.id = 0;
        novaLiga.flagActive = 1;
        novaLiga.dataInitial = new Date();
        novaLiga.image = '';
        this.loadLiga(novaLiga);
      }

    });

  }

  loadLiga(_liga: LigasModel){
    
    this.liga = _liga;
    this.ligaOld = Object.assign({},_liga); //Usa caso queira resetar o formulario

    this.ligaForm = this.ligaFB.group({
      name: [this.liga.name, Validators.required],
      image: [''],
      flagActive: [this.liga.flagActive, Validators.required],
      observation: [this.liga.observation],
      dataInitial: [this.liga.dataInitial]
    });
 
    if (this.liga.id > 0){
      this.montarImagem();
    }

    this.ligaForm.controls.dataInitial.disable();
  }

  voltarPagina(){
    this.router.navigate(['ligas/lista']);
  }

  validacao(): boolean{
    this.existeErro = false;
    const controls = this.ligaForm.controls;

    if (this.ligaForm.invalid){
      Object.keys(controls).forEach(controlName => 
        controls[controlName].markAllAsTouched()
      );

      this.existeErro = true;
      return false;
    }

    return true;

  }

  prepararModel(): LigasModel {
    const controls = this.ligaForm.controls;
    const _ligaModel = new LigasModel();

    _ligaModel.id = this.liga.id;
    _ligaModel.name = controls.name.value;
    _ligaModel.observation = controls.observation.value;
    _ligaModel.image = this.arquivoAdd != '' ? controls.image.value : this.liga.image;
    _ligaModel.flagActive = controls.flagActive.value;
    _ligaModel.dataInitial = controls.dataInitial.value;
    _ligaModel.clienteId = 1;
    return _ligaModel;
  }

  Salvar(){
    if (this.validacao() === false)
      return;

    let ligaModelPreparado = this.prepararModel();

    if (ligaModelPreparado.id > 0){
      this.Atualizar(ligaModelPreparado);
      return;
    }

    this.Adicionar(ligaModelPreparado);

  }

  Adicionar(_liga: LigasModel){
    this.ligaService.adicionar(_liga).subscribe((result : any) => {
      debugger;
      if (!result){
        this.toast.error('Erro ao realizar o cadastro');
        return;
      }

      if (this.file) {
        this.uploadImagem(result.dados.id, result.dados.clienteId).subscribe();
      }

      this.toast.success('Cadastro realizado com sucesso!');
      this.router.navigate(['ligas/lista']);
    })
  
  }

  Atualizar(_liga: LigasModel){

    this.ligaService.atualizar(_liga).subscribe((result : any) => {
      if (!result){
        this.toast.error('Erro ao realizar o alteração');
        return;
      }

      if (this.file) {
        this.uploadImagem(result.dados.id, result.dados.clienteId).subscribe();
      }

      this.toast.success('Alteração realizado com sucesso!');
      this.router.navigate(['ligas/lista']);
    })
  }

  BuscaPorId(id: number){
    this.ligaService.obterPorId(id).subscribe((result: any) => {
      if (!result){
        return;
      }

      this.loadLiga(result.dados);
    } )
  }

  uploadImagem(id: number, clientId: number) {
    let upload = new UploadModel();
    upload.id = id;
    upload.nomeArquivo = this.montarNomeImageUpload(id, clientId);
    upload.uploadPasta = enUploadPasta.Liga;
    return this.uploadService.upload(this.file, upload, enUploadPasta.Liga.toString());

  }

  montarNomeImageUpload(id: number, clientId: number) {
    if (this.file) {
      return ` ${id}-${clientId}.${this.file[0].name.split('.')[1]}`;
    }
    return '';
  }

  onFileChange(event) {

		if (event.target.files && event.target.files.length) {
      this.file = event.target.files;
      this.arquivoAdd = this.file[0].name;
      this.previewImagem(this.file);
		}

  }
  
  previewImagem(file: File) {
    if (!file && file.size === 0) return;
    const fileReader = new FileReader();
    fileReader.onloadend = () => {
      this.base64Image = fileReader.result.toString();
      this.nomeImagemPrevisualizacao = file[0].name;
    };
    fileReader.readAsDataURL(file[0]);
  }

  montarImagem() {
    if (this.liga && this.liga.image !== '')
      this.urlImagem = `${environment.url_image}Liga/${this.liga.image}`;
    else
      this.urlImagem = '';
  }

}
