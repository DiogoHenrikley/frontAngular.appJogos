import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LigasComponent } from './ligas.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BoxModule } from 'angular-admin-lte';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ComponentsModule } from '../components/components.module';
import { LigasListaComponent } from './ligas-lista/ligas-lista.component';
import { LigasCadastroComponent } from './ligas-cadastro/ligas-cadastro.component';
import { MatIconModule } from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from '../core/config/format-datepicker';
import { AlertModule as MkAlertModule } from 'angular-admin-lte';
import { MatTabsModule } from '@angular/material/tabs';
import { LigasService } from '../core/services/ligas.service';
import { AuthInterceptor } from '../core/auth/auth.interceptor';
import { LoaderInterceptorService } from '../core/layout/loader/loader.interceptor';
import { MatCardModule } from '@angular/material/card';

const routes : Routes = [
  {
    path: '',
    component: LigasComponent,
    children : [
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      },
      {
        path: 'lista',
        component: LigasListaComponent,
        data: {
          title: 'Lista de Ligas'
        }
      },
      {
        path: 'cadastro',
        component: LigasCadastroComponent,
        data: {
          title: 'Cadastro de Ligas'
        }
      },
      {
        path: 'cadastro/edit/:id',
        component: LigasCadastroComponent,
        data: {
          title: 'Editar Liga'
        }
      },
      {
        path: 'cadastro/add',
        component: LigasCadastroComponent,
        data: {
          title: 'Cadastro de Ligas'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    BoxModule,
    MatInputModule,
    MatButtonModule,
    MatInputModule,
    ComponentsModule,
    MatIconModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MkAlertModule,
    MatTabsModule,
    MatCardModule
  ],
  declarations: [
    LigasComponent,
    LigasListaComponent,
    LigasCadastroComponent
  ],
  providers:[
    MatDatepickerModule,
    MatNativeDateModule,
    LigasService,
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    }
  ]
})
export class LigasModule { }
