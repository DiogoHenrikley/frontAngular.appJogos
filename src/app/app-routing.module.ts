import { title } from 'process';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AuthGuard } from './core/auth/auth.guard';
import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
    data: {
      customLayout: true
    }
  }, 
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then(m => m.RegisterModule),
    data: {
      customLayout: true
    }
  },
  {
    path: '',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    data: {
      title: 'Inicio'
    },
    children: [
      {
        path: 'home',
        component: HomeComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'membros',
        loadChildren: () => import('./membros/membros.module').then(m => m.MembrosModule),
        data: {
          title: 'Membros'
        }
      },
      {
        path: 'ligas',
        loadChildren: () => import('./ligas/ligas.module').then(m => m.LigasModule),
        data: {
          title: 'Ligas'
        }
      },
      {
        path:'negociacoes',
        loadChildren:()=> import('./negociacoes/negociacoes.module').then(m=> m.NegociacoesModule),
        data:{
          title:'Negociações'
        }
      },
      {
        path:'amigos',
        loadChildren:() => import('./Amigos/amigos.module').then(m=> m.AmigoModule),
        data:{
          title:'Amigos'
        }
      },
      {
        path:'jogos',
        loadChildren:() => import('./Jogos/jogos.module').then(m=> m.JogosModule),
        data:{
          title:'Jogos'
        }

      },
      {
        path:'emprestimo',
        loadChildren:() => import('./emprestimos/emprestimo.module').then(m=> m.EmprestimoModule),
        data:{
          title:'Controle de jogos'
        }
      }
    ]
  },
  {path: '**', redirectTo: 'login', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
