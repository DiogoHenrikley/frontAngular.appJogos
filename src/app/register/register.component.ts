import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  modelo: any = {};
  constructor(private HttpClient: HttpClient,
    private toastService: ToastrService,
    private router: Router) { }

  url() {
    return `${environment.api_url}/api/Login`;
  }

  ngOnInit() {
    this.modelo = {};
  }

  registarUser() {
    if (this.validarForm()) {
      this.HttpClient.post(`${this.url()}/GerarUsuario`, this.modelo).subscribe((result: any) => {
        if (result.success) {
          this.toastService.success('Usuário registrado com sucesso');
          this.router.navigate(['/login']);
        } else
          this.toastService.error('Erro ao registrar usuário.', 'Registro de usuário');
      });
    }
  }

  validarForm() {
    let mensagem = '';
    if (this.modelo.nome === undefined || this.modelo.nome === '') {
      mensagem += 'O nome completo deve ser preenchido \n';
    }
    if (this.modelo.email === undefined || this.modelo.emai === '') {
      mensagem += 'O email deve ser preenchido e válido \n';
    }
    if (this.modelo.senha === undefined || this.modelo.senha === '') {
      mensagem += 'A senha deve ser preenchida \n';
    }

    if (this.modelo.senha !== this.modelo.confirmarsenha) {
      mensagem += 'As senhas devem ser iguais. \n';
    }
    if (this.modelo.senha.length <= 3) {
      mensagem += 'A senha dever ser maior do que 3 caracteres';
    }
    if (mensagem !== '') {
      this.toastService.error(mensagem, 'Registar usário');
      return false;
    }
    return true;
  }


}
