export const adminLteConf = {
  skin: 'blue',
  // isSidebarLeftCollapsed: false,
  // isSidebarLeftExpandOnOver: false,
  // isSidebarLeftMouseOver: false,
  // isSidebarLeftMini: true,
  // sidebarRightSkin: 'dark',
  //isSidebarRightCollapsed: false,
  //isSidebarRightOverContent: false,
  layout: 'normal',
  sidebarLeftMenu: [
    { label: 'MENU', separator: true },

    {
      label: 'Gerenciar jogos', iconClasses: 'fa fa-tasks', children: [
        {
          label: 'Amigos', route: 'amigos'
        },{
          label:'Jogos', route:'jogos'
        },{
          label:'Jogos emprestados',route:'emprestimo'
        }
      ]
    },
    {
      label:'Logout',iconsClasses:'',children:[
        {label:'Sair',route:'sair'}
      ]
    }
  ]
};
