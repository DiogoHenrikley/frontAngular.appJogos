
export class Util {

    static obterObjetoUser() {
        const user =  JSON.parse(atob(localStorage.getItem("user")));
        return user;
    }
    
    static obterIdCliente(){
        const user = JSON.parse(atob(localStorage.getItem("user")));
        return +user.idCliente;
    }
    
    /**
     * @author Diogo Henrikley 
     * @function Obter campo texto do Mat-Select
     * Método para obter a descrição do campo text em um Mat-Select
     * @param event Evento disparado no componente.
     */
    static obterTextMatSelect(event:any){
        return (event.source.selected._element.nativeElement).innerText.trim();
    }
}