import { Component } from '@angular/core';
import { LoginService } from '../../services/login.service';
@Component({
  selector: 'app-header-inner',
  templateUrl: './header-inner.component.html'
})
export class HeaderInnerComponent {
  
  constructor(public loginService: LoginService) {

  }

  logout(e){
    e.preventDefault();
    this.loginService.logout();
  }
}
