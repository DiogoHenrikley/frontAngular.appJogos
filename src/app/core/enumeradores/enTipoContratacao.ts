export enum enTipoContratacao {
    Emprestimo = 1,
    Definitivo = 2,
    Roubar = 3,
    AjustarSalario = 4
}

export const TraduzirEnTipoContrato = (tipo: number) => {
    switch (tipo) {
        case enTipoContratacao.Emprestimo:
            return 'Emprestimo';
        case enTipoContratacao.Definitivo:
            return 'Definitivo';
        case enTipoContratacao.Roubar:
            return 'Roubar na multa';
        case enTipoContratacao.AjustarSalario:
            return 'Ajustar salário';
        default:
            return 'Não identificado';
    }
}
