export enum enSexo {
    Masculino = 1,
    Feminino = 2
}

 export const TraduzirEnumSexo  = (tipo:enSexo) =>{
     switch (tipo) {
         case enSexo.Masculino:
            return "Masculino"             ;
        case enSexo.Feminino:
            return "Feminino";
     }
 }