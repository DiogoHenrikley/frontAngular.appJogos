export enum enHabilidade{
    semSelecao = 0,
    talentoOfensivo = 1,
    controleDeBola = 2,
    drible = 3,
    passeRasteiro = 4,
    passePeloAlto = 5,
    finalizacao = 6,
    chuteColocado = 7,
    giroControlado = 8,
    cabecada = 9,
    talentoDefensivo = 10,
    desarme = 11,
    forcaDoChute = 12,
    velocidade = 13,
    explosao = 14,
    equilibrio = 15,
    contatoFisico = 16,
    impulsao = 17,
    resistencia = 18,
    talentoComoGoleiro = 19,
    catching = 20,
    clearing = 21,
    reflexes = 22,
    coverage = 23,
    condicaoFisica = 24,
    resistenciaLesao = 25,
    piorPeFrequencia = 26,
    piorPePrecisao = 27,
    agressividade = 28,
    conducaoFirme = 29   
}