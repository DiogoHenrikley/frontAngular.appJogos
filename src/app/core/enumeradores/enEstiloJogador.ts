export enum enEstiloJogador {
    Acao = 0,
    Aventura = 1,
    Corrida = 2,
    Futebol = 3,
    Guerra = 4,
    Terror = 5,
    Outros = 6
}

export const TraduzirEstiloJogo = (tipo:enEstiloJogador) =>{
    return enEstiloJogador[tipo];
}