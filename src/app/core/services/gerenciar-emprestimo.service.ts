import { EmprestimoModel } from './../models/emprestimo.mode';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GerenciarEmprestimoService {

  url() {
    return `${environment.api_url}/api/GerenciadorEmprestimo`;
  }

  constructor(private httpCliente: HttpClient) { }

  obterTodos() {
    return this.httpCliente.get(this.url());
  }

  gerenciarInsertUpdate(emprestimoModel: EmprestimoModel) {
    if (emprestimoModel.id === undefined || emprestimoModel.id > 0)
      return this.salvar(emprestimoModel);
    return this.atualizar(emprestimoModel);

  }

  salvar(emprestimoModel: EmprestimoModel) {
    return this.httpCliente.post(this.url(), emprestimoModel);
  }

  atualizar(emprestimoModel: EmprestimoModel) {
    return this.httpCliente.put(this.url(), emprestimoModel);
  }
  remover(id: number) {
    return this.httpCliente.delete(`${this.url()}/${id}`);
  }

  obterPorId(id: number) {
    return this.httpCliente.get(`${this.url()}/${id}`);
  }

  obterPorDescricao(descricao: number) {
    return this.httpCliente.get(`${this.url()}/ObterPorDescricao/${descricao}`);
  }

}
