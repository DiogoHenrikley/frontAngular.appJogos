import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  constructor(private httpClient:HttpClient) { }
  url(){
    return `${environment.api_url}/api/Estado`;
  }

  obterEstados(){
    return this.httpClient.get(`${this.url()}`);
  }

}
