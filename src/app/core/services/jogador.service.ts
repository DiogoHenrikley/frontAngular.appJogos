import { FiltroNegociacoes } from './../filtros/filtro.negociacoes';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class JogadorService {

  url(){
    return `${environment.api_url}/api/Jogador`;
  }

  constructor(private httpClient:HttpClient) { }

  obterJogador(){
    return this.httpClient.get(`${this.url()}`);
  }

  obterJogadorPorFiltro(filtro:FiltroNegociacoes){
    return this.httpClient.post(`${this.url()}/obterJogadorPorFiltro`,filtro);
  }

}
