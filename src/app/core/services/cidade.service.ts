import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class CidadeService {
  url(){
    return `${environment.api_url}/api/Cidade`;
  }
  constructor(public httpCliente:HttpClient) { }

  obterCidadePorIdEstado(EstadoId:number){
    return this.httpCliente.get(`${this.url()}/getCityByIdState/${EstadoId}`);
  }


}
