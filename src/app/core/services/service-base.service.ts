import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export abstract class ServiceBaseService {

  constructor(private httpClient: HttpClient) { }

  /**
   * 
   * @param controller controller que será passado.
   */
  public  URL(controller: string): string {
    return `${environment.api_url}/controller`;
  }

  public post<T>(): Observable<T> {
    return null;
  }
  public getAll<T>(): Observable<T> {
    return null;
  }

  public delete(id: number) {
    return null;
  }

  public put<T>(dados: any): Observable<T> {
    return null;
  }

}
