import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LigasModel } from '../models/ligas.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LigasService {

  constructor(private httpClient: HttpClient) { }

  url(){
    return `${environment.api_url}/api/Liga`;
  }

  obterTodos(): Observable<LigasModel[]> {
    return this.httpClient.get<LigasModel[]>(`${this.url()}`);
  }

  obterPorDescricao(nome: String = ''): Observable<LigasModel[]>{
    return this.httpClient.get<LigasModel[]>(`${this.url()}/getByName/${nome}`);
  }

  obterPorId(id:number):Observable<LigasModel>{
    return this.httpClient.get<LigasModel>(`${this.url()}/${id}`);
  }

  adicionar(membro:LigasModel){
    return this.httpClient.post<LigasModel>(`${this.url()}`,membro);
  }

  atualizar(membro:LigasModel){
    return this.httpClient.put<LigasModel>(`${this.url()}`,membro);
  }


}
