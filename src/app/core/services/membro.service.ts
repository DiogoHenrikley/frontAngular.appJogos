import { EscudoModel } from './../models/escudo.model';
import { User } from './../models/user.model';
import { environment } from './../../../environments/environment';
import { MembroModel } from './../models/membro.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MembroService {
  url() {
    return `${environment.api_url}/api/Membro`;
  }

  constructor(private httpClient: HttpClient) {
  }

  obterTodos(): Observable<MembroModel[]> {
    return this.httpClient.get<MembroModel[]>(`${this.url()}/obterTodos`);
  }

  obterPorId(id: number): Observable<MembroModel> {
    return this.httpClient.get<MembroModel>(`${this.url()}/${id}`)
  }

  obterPorDescricao(Nome: string): Observable<MembroModel[]> {
    return this.httpClient.get<MembroModel[]>(`${this.url()}/getbyNome/${Nome}`);
  }

  salvar(membro: MembroModel) {
    return this.httpClient.post<MembroModel>(`${this.url()}`, membro);
  }

  atualizar(membro: MembroModel) {
    return this.httpClient.put<MembroModel>(this.url(), membro);
  }

  controladorOperacaoSalvarAtualizar(membro: MembroModel) {
    if (membro.id === undefined  ||  membro.id === 0)
      return this.salvar(membro);
    else
      return this.atualizar(membro);
  }

  autenticar(credentials: { email: string, password: string }) {
    return this.httpClient.get(`${this.url()}/Auth/${credentials.email}/${credentials.password}`);
  }

  ativar(id: number, status: boolean, motivo: string) {
    return this.httpClient.put(`${this.url()}/ativar`, { id: id, active: status, observation: motivo });
  }

  upload(file: File, fileName: string) {
    const fileToUpload = <File>file[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileName);

    return this.httpClient.post(`${this.url()}/upload`, formData);
  }

  obterMembroPorEscudoVinculado(idCliente:number){
    return this.httpClient.get(`${this.url()}/obterMembroPorEscudoVinculado/${idCliente}`);
  }

}
