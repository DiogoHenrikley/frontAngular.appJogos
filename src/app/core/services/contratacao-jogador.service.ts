import { ContratacaoJogadorModel } from './../models/contratacao.jogador.model';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContratacaoJogadorService {
  url() {
    return `${environment.api_url}/api/Contratacao`;
  }
  constructor(private httClient: HttpClient) { }

  gerenciarContratacao(jogadorModel: ContratacaoJogadorModel) {
    if ( jogadorModel.Id === undefined ||  jogadorModel.Id === 0)
      return this.salvar(jogadorModel);
    else
      return this.alterar(jogadorModel);
  }

  salvar(jogador: ContratacaoJogadorModel) {
    return this.httClient.post(`${this.url()}`, jogador);
  }

  alterar(jogador: ContratacaoJogadorModel) {
    return this.httClient.put(`${this.url()}`, jogador);
  }

  excluir(id: number) {
    return this.httClient.delete(`${this.url()}/excluir/${id}`);
  }

}
