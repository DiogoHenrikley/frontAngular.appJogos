import { LoginModel } from './../models/login.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  constructor(private http: HttpClient, private router: Router) { }

  url(){
    return `${environment.api_url}/api/Login`;
  }

  check(): boolean{
    //return localStorage.getItem('user') ? true : false;
    return true;
  }

  logout(): void {
    localStorage.clear();
    this.router.navigateByUrl('/login'); 
  }
  
  autenticar(credentials: { email: string, senha: string }): Observable<LoginModel> {
    return this.http.post<LoginModel>(`${this.url()}/Autenticar`, credentials);
  }

  gerarlogin(modelo:any){
    return this.http.post(`${this.url()}/GerarUsuario`,modelo);
  }
  getUser(): User {
    return localStorage.getItem('user') ? JSON.parse(atob(localStorage.getItem('user'))) : new User();
  }

}
