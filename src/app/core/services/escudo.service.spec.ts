import { TestBed } from '@angular/core/testing';

import { EscudoService } from './escudo.service';

describe('EscudoService', () => {
  let service: EscudoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EscudoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
