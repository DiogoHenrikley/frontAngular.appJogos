import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EscudoService {

  url(){
    return `${environment.api_url}/api/Escudo`;
  }

  constructor(private httpClient:HttpClient) { }

  obterEscudos(){
    return this.httpClient.get(`${this.url()}`);
  }
  obterEscudoAtivo(){
    return this.httpClient.get(`${this.url()}/obterEscudoAtivo`);
  }

}
