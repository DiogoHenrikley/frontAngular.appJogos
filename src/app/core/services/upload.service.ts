import { UploadModel } from './../models/upload.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { enUploadPasta } from '../enumeradores/enuploadPasta';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  url(){
    return `${environment.api_url}/api/Upload`;
  }
  constructor(private httpClient:HttpClient) { }

  upload(file:File,upload:UploadModel, enumPasta: string = enUploadPasta.Membros.toString() ){
    const fileToUpload = <File> file[0];
    const formData = new FormData();
    formData.append('file',fileToUpload,upload.nomeArquivo);
    formData.append(enumPasta, new Blob(['Membros']),'pastaupload');
    formData.append(upload.id.toString(), new Blob([`${upload.id.toString()}`]) ,'idmembro');

    console.log(`${this.url()}/uploadImagem`);
    
    return this.httpClient.post(`${this.url()}/uploadImagem`,formData);
  }

}
