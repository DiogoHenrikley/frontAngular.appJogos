import { JogosModel } from './../models/jogos.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ElementSchemaRegistry } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class JogosService {
  
  constructor(public httpClient:HttpClient) { }
  
  url() {
    return `${environment.api_url}/api/Jogos`;
  }

  obterTodos(): Observable<JogosModel[]> {
    return this.httpClient.get<JogosModel[]>(`${this.url()}`);
  }

  obterPorId(id: number): Observable<JogosModel> {
    return this.httpClient.get<JogosModel>(`${this.url()}/${id}`)
  }

  obterPorDescricao(Nome: string): Observable<JogosModel[]> {
    return this.httpClient.get<JogosModel[]>(`${this.url()}/obterPorDescricao/${Nome}`);
  }

  salvar(jogosModel: JogosModel) {
    return this.httpClient.post<JogosModel>(`${this.url()}`, jogosModel);
  }

  atualizar(jogosModel: JogosModel) {
    return this.httpClient.put<JogosModel>(this.url(), jogosModel);
  }

  remover(id:number){
    return this.httpClient.delete(`${this.url()}/${id}`);
  }

  gerenciarInsertUpdate(jogosModel:JogosModel){
    if(jogosModel.id === undefined || jogosModel.id === 0)
    return this.salvar(jogosModel);
    else 
    return this.atualizar(jogosModel);
  }

  
}
