import { JogosModel } from './../models/jogos.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmprestimosService {

  url() {
    return `${environment.api_url}/Amigos`;
  }

  constructor(public httpclient: HttpClient) {

  }

  /**
   * @author Diogo Henrikley
   * @method Obter todos os amigos cadastrados no banco
   */
  public obterTodos() {
    return this.httpclient.get(this.url());
  }

  /**
 * @author Diogo Henrikley
 * @method Obter todos os amigos cadastrados no banco por id
 * @param id
 */

  public obterPorId(id: number) {
    return this.httpclient.get(`${this.url()}/id`);
  }
  /**
   * @author Diogo Henrikley
   * @method Obter todos os amigos cadastrados no banco
   * @param descricao
   * @returns lista de objetos referente ao filtro descrição.
   */

  public obterPorDescricao(descricao: string) {
    return this.httpclient.get(`${this.url()}/${descricao}`);
  }
  /**
   * @author Diogo Henrikley
   * @method Obter todos os amigos cadastrados no banco
   * @param modelo Objeto do tipo AmigoModel que será persistido no banco.
   * @returns objeto informando successo ou falha {success = true/false}
   */

  public gerenciarInsertUpdate(modelo: JogosModel) {
    if (modelo.id !== undefined && modelo.id === 0)
      return this.salvar(modelo);

    return this.atualizar(modelo);
  }

  /**
 * @author Diogo Henrikley
 * @method Obter todos os amigos cadastrados no banco
 * @param id Id do item que será removido.
 * @returns objeto informando successo ou falha {success = true/false}
 */
  public remover(id: number) {
    return this.httpclient.delete(`${this.url()}/${id}`);
  }

  /**
 * @author Diogo Henrikley
 * @method salvar todos os amigos cadastrados no banco
 * @param id Id do item que será removido.
 * @returns objeto informando successo ou falha {success = true/false}
 */  
  private salvar(modelo: JogosModel) {
    return this.httpclient.post(`${this.url()}`, modelo);
  }
    /**
 * @author Diogo Henrikley
 * @method Obter todos os amigos cadastrados no banco
 * @param id Id do item que será removido.
 * @returns objeto informando successo ou falha {success = true/false}
 */
  private atualizar(modelo: JogosModel) {
    return this.httpclient.put(`${this.url()}`, modelo);
  }

}
