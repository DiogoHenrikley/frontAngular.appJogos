import { AmigoModel } from './../models/amigo.model';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AmigosService {

  url() {
    return `${environment.api_url}/api/Amigos`;
  }

  constructor(private httpclient: HttpClient) {

  }

  /**
   * @author Diogo Henrikley
   * @method Obter todos os amigos cadastrados no banco
   */
  public obterTodos() {
    return this.httpclient.get(this.url());
  }

  /**
 * @author Diogo Henrikley
 * @method Obter todos os amigos cadastrados no banco por id
 * @param id
 */

  public obterPorId(id: number) {
    return this.httpclient.get(`${this.url()}/${id}`);
  }
  /**
   * @author Diogo Henrikley
   * @method Obter todos os amigos cadastrados no banco
   * @param descricao
   * @returns lista de objetos referente ao filtro descrição.
   */

  public obterPorDescricao(descricao: string) {
    return this.httpclient.get(`${this.url()}/obterPorDescricao/${descricao}`);
  }
  /**
   * @author Diogo Henrikley
   * @method Obter todos os amigos cadastrados no banco
   * @param modelo Objeto do tipo AmigoModel que será persistido no banco.
   * @returns objeto informando successo ou falha {success = true/false}
   */

  public gerenciarInsertUpdate(modelo: AmigoModel) {
    if (modelo.id === undefined || modelo.id === 0)
      return this.salvar(modelo);

    return this.atualizar(modelo);
  }

  /**
 * @author Diogo Henrikley
 * @method Obter todos os amigos cadastrados no banco
 * @param id Id do item que será removido.
 * @returns objeto informando successo ou falha {success = true/false}
 */
  public remover(id: number) {
    return this.httpclient.delete(`${this.url()}/${id}`);
  }

  /**
 * @author Diogo Henrikley
 * @method salvar todos os amigos cadastrados no banco
 * @param id Id do item que será removido.
 * @returns objeto informando successo ou falha {success = true/false}
 */  
  private salvar(modelo: AmigoModel) {
    return this.httpclient.post(`${this.url()}`, modelo);
  }
    /**
 * @author Diogo Henrikley
 * @method Obter todos os amigos cadastrados no banco
 * @param id Id do item que será removido.
 * @returns objeto informando successo ou falha {success = true/false}
 */
  private atualizar(modelo: AmigoModel) {
    return this.httpclient.put(`${this.url()}`, modelo);
  }


}
