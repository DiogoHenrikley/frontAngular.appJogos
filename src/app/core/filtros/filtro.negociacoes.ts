import { enHabilidade } from './../enumeradores/enHabilidade';
export class FiltroNegociacoes{
    public nomeJogador:string='';
    public posicaoJogador:string='';
    public idTimeLiga:number=0;
    public idTimeEscudo:number=0;
    public habilidade : enHabilidade;
    public habilidadeInicial:number=0;
    public habilidadeFinal:number=0;
    public idadeInicial:number=0;
    public idadeFinal:number=0;
    public overRallFinal:number=0;
    public overRallInicial:number=0;
    public comprarNaMulta:number=-1;
    public contrato:number=-1;
    public emprestimo:number=-1;
    public passeInicial:number=0;
    public passeFinal:number=0;

}