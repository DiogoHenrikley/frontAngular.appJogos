import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BoxModule, TabsModule, DropdownModule } from 'angular-admin-lte';

import { HeaderInnerComponent } from './layout/header-inner/header-inner.component';
import { SidebarLeftInnerComponent } from './layout/sidebar-left-inner/sidebar-left-inner.component';
import { SidebarRightInnerComponent } from './layout/sidebar-right-inner/sidebar-right-inner.component';
import { LoaderComponent } from './layout/loader/loader.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    DropdownModule,
    TabsModule,
    BoxModule,
    MatProgressSpinnerModule
  ],
  declarations: [HeaderInnerComponent, SidebarLeftInnerComponent, SidebarRightInnerComponent, LoaderComponent],
  exports: [BoxModule, TabsModule, HeaderInnerComponent, SidebarLeftInnerComponent, SidebarRightInnerComponent,LoaderComponent ]
})
export class CoreModule { }
