import { TraduzirEstiloJogo } from './../enumeradores/enEstiloJogador';
import { enEstiloJogador } from '../enumeradores/enEstiloJogador';
import { AmigoModel } from './amigo.model';
import { JogosModel } from './jogos.model';

export class EmprestimoModel{
    public id:number;
    public amigoId:number;
    public nomeAmigo:string;
    public jogoId:number;
    public nomejogo:string;
    public estilojogo:enEstiloJogador;
    public statusjogo:string;
  
}