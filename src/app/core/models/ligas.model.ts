export class LigasModel {
    id: number;
    name: string;
    image: string;
    flagActive: number;
    observation: string;
    dataInitial: Date;
    dataRemove: Date;
    dataUpdate: Date;
    clienteId: number;
}

