export class MembroModel {
    public id: number =0;
    public clienteId: number;
    public descCliente: string ='';
    public name: string ='';
    public username: string='';
    public urlImagem:string='';
    public image: string='';
    public cidadeId: number;
    public descCidade: string='';
    public estadoId: number;
    public descEstado: string='';
    public levelId: number;
    public descLevel: string='';
    public active: boolean= true;
    public escudoId: number;
    public descShield: string='';
    public division: number=0;
    public account: number=0;
    public email: string='';
    public password: string='';
    public confirmPassword:string='';
    public emailVerified:Date;
    public mobile: string='';
    public gender: number=0;
    public descGender: string='';
    public dateOfBirth: Date;
    public subscription: number=0;
    public sponsorship: number=0;
    public proSteals: boolean=false;
    public proNegotiate: boolean=false;
    public proUserStealing: boolean=false;
    public grades: string='';
    public rememberToken: string='';
    public observation: string ='';   
    public carregouImagem :boolean=false;
    // constructor(
    //     public id: number,
    //     public clienteId: number,
    //     public descCliente: string,
    //     public name: string,
    //     public username: string,
    //     public image: string,
    //     public cidadeId: number,
    //     public descCidade: string,
    //     public estadoId: number,
    //     public descEstado: string,
    //     public levelId: number,
    //     public descLevel: string,
    //     public active: boolean,
    //     public escudoId: number,
    //     public descShield: string,
    //     public division: number,
    //     public account: number,
    //     public email: string,
    //     public password: string,
    //     public emailVerified: Date,
    //     public mobile: string,
    //     public gender: number,
    //     public descGender: string,
    //     public dateOfBirth: Date,
    //     public subscription: string,
    //     public sponsorship: number,
    //     public proSteals: boolean,
    //     public proNegotiate: boolean,
    //     public proUserStealing: boolean,
    //     public grades: string,
    //     public rememberToken: string,
    //     public observation: string
    // ) {

    // }
    
}