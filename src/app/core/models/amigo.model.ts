import { stringify } from 'querystring';
import { enSexo, TraduzirEnumSexo } from './../enumeradores/enSexo';
export class AmigoModel {

    public id: number;
    public nome: string;
    public sexo: enSexo;
    public descricaoSexo:string 
    toString() {
        return `${this.id} - ${this.nome} - ${TraduzirEnumSexo(this.sexo)}`
    }

    public  isValid(){
        return (this.nome !== undefined && this.nome !== '');
    }
}