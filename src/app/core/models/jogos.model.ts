import { enEstiloJogador } from "../enumeradores/enEstiloJogador";

export class JogosModel {
    public id: number;
    public nome: string;
    public estiloJogo: enEstiloJogador;
    public observacao: string;

    toString() {
        return `${this.id} - ${this.nome} - ${this.estiloJogo} - ${this.observacao}`;
    }
}