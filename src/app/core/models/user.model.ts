import { EscudoModel } from "./escudo.model";

export class User {
    id: number;
    userName?: string;
    email: string;
    image:string;
    token: string;
    escudo:EscudoModel
}
