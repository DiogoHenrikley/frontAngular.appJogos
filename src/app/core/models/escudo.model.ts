export class EscudoModel {
    constructor(public id: number,
        public idPais: number,
        public name: string,
        public abbreviated: string,
        public statuim: string,
        public image: string,
        public active: number,
        public observation) {
    }
}