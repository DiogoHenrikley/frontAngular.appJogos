import { enUploadPasta } from './../enumeradores/enuploadPasta';
export class UploadModel{
    public id:number;
    public uploadPasta:enUploadPasta;
    public nomeArquivo:string;
    public extensao :string;
    public formData:FormData;
}