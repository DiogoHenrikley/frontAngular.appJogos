import { enTipoContratacao } from './../enumeradores/enTipoContratacao';
export class ContratacaoJogadorModel{
    public Id:number;
    public membroId:number;
    public jogadorId:number;
    public nomeJogador:string='';
    public status:enTipoContratacao;
    public statusDescricao:string;
    public valorPasse:number=0;
    public valorMinimo:number=0;
    public valorPadrao:number=0;
    public observacao:string='';
}