import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { JogosService } from './../../core/services/jogos.service';
import { TraduzirEstiloJogo } from './../../core/enumeradores/enEstiloJogador';
import { JogosModel } from './../../core/models/jogos.model';
import { DataTableColunas } from './../../components/_models/DataTableColunas';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DataTableAcoes } from 'src/app/components/_models/DataTableAcoes';

@Component({
  selector: 'app-jogos-lista',
  templateUrl: './jogos-lista.component.html',
  styleUrls: ['./jogos-lista.component.css']
})
export class JogosListaComponent implements OnInit {

  filtroNome = '';
  jogosModel = new JogosModel();

  colunas: DataTableColunas[] = [
    { propriedade: 'id', titulo: 'Id', disabled: false, cell: (row: JogosModel) => `${row.id}` },
    { propriedade: 'nome', titulo: 'Nome', disabled: false, cell: (row: JogosModel) => `${row.nome}` },
    { propriedade: 'estilojogo', titulo: 'Estilo', disabled: false, cell: (row: JogosModel) => TraduzirEstiloJogo(row.estiloJogo) },
  ];

  acoes: DataTableAcoes[] = [
    { icone: 'create', evento: this.editar.bind(this), toolTip: 'Editar', color: 'primary' },
    { icone: 'delete', evento: this.remover.bind(this), toolTip: 'Deletar', color: 'warn' }
  ];

  dadosTabela: JogosModel[] = [];

  constructor(public route: Router,
    private jogosService: JogosService,
    private toastService: ToastrService) { }

  ngOnInit(): void {
    this.buscar();
  }

  navegar() {
    this.route.navigate(['jogos/cadastro']);
  }

  remover(jogosModel: JogosModel) {
    Swal.fire({
      cancelButtonText: 'Cancelar',
      showCancelButton: true,
      confirmButtonText: 'OK',
      text: `Gostaria de excluir o jogo? ${jogosModel.nome}`,
      titleText: 'Excluir Jogos'
    }).then((result: any) => {
      if (result.isConfirmed) {
        this.jogosService.remover(jogosModel.id).subscribe((result: any) => {
          if (result.success) {
            this.toastService.success('Operação realizada com sucesso', 'Excluir Jogos');
            debugger;
            this.removeItemPorIndex(jogosModel);
          }
          else
            this.toastService.error('Erro ao realizar esta operação', 'Excluir Jogos');
        });
      }
    });
  }

  removeItemPorIndex(jogosModel: JogosModel) {
    this.dadosTabela = this.dadosTabela.filter(x => x.id !== jogosModel.id);
  }

  editar(jogosModel: JogosModel) {
    this.route.navigate([`jogos/cadastro/${jogosModel.id}`]);
  }

  buscar() {

    const valor = Number.parseInt(this.filtroNome);

    if (valor) {
      this.jogosService.obterPorId( Number.parseInt(this.filtroNome)).subscribe((result: any) => {
        const lista:  JogosModel[] = [];
        lista.push(result.dados);
        this.dadosTabela = lista;
        return;
      })
    } else if (this.filtroNome !== '') {
      this.jogosService.obterPorDescricao(this.filtroNome).subscribe((result: any) => {
        this.dadosTabela = result.dados;
        return;
      });

      this.jogosService.obterTodos().subscribe((result:any)=>{
        this.dadosTabela = result.dados;
      });
      
    }

    this.jogosService.obterTodos().subscribe((result: any) => {
      this.dadosTabela = result.dados;
    });
  }

  adicionar() {
    this.route.navigate(['jogos/cadastro']);
  }

}
