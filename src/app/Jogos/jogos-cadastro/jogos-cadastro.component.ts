import { ToastrService } from 'ngx-toastr';
import { JogosService } from './../../core/services/jogos.service';
import { Router, ActivatedRoute } from '@angular/router';
import { JogosModel } from './../../core/models/jogos.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { param } from 'jquery';

@Component({
  selector: 'app-jogos-cadastro',
  templateUrl: './jogos-cadastro.component.html',
  styleUrls: ['./jogos-cadastro.component.css']
})
export class JogosCadastroComponent implements OnInit {

  jogosForm: FormGroup;
  jogosModel = new JogosModel();
  existeErro = false;

  estiloJogos = [
    { id: 1, name: 'ação' },
    { id: 2, name: 'Aventura' },
    { id: 3, name: 'Futebol' },
    { id: 4, name: 'Guerra' },
    { id: 5, name: 'Terror' },
    { id: 6, name: 'Outros' }
  ];

  TITULO_MENSAGEM = 'Cadastro de Jogos';

  constructor(public jogosFB: FormBuilder,
    public route: Router,
    private jogoService: JogosService,
    private toastService: ToastrService,
    private activeRouter: ActivatedRoute) { }

  ngOnInit(): void {
    this.activeRouter.params.subscribe((result: any) => {
      const id = result.id;
      if (id !== undefined || id > 0) {
        this.obterPorId(id);
        //this.loadJogos(this.jogosModel);
      }
    });

    this.loadJogos(this.jogosModel);
  }

  loadJogos(_jogosModel: JogosModel) {
    this.jogosModel = _jogosModel;

    this.jogosForm = this.jogosFB.group({
      nome: [this.jogosModel.nome, Validators.required],
      estiloJogo: [this.jogosModel.estiloJogo, Validators.required],
      observacao: [this.jogosModel.observacao],
    });
    this.jogosForm.controls['estiloJogo'].setValue(this.jogosModel.estiloJogo);
  }

  obterPorId(id: number) {
    this.jogoService.obterPorId(id).subscribe((result: any) => {
      debugger;
      this.jogosModel = result.dados as JogosModel;
      this.loadJogos(this.jogosModel);
    })
  }

  salvar() {
    if (!this.validacao()) return;
    this.prepararModel();
    this.jogoService.gerenciarInsertUpdate(this.jogosModel).subscribe((result: any) => {
      if (result.success){
        this.toastService.success('Operação realizada com sucesso', this.TITULO_MENSAGEM);
        this.loadJogos(new JogosModel());
      }
      else
        this.toastService.error('Erro ao realizar essa operação', this.TITULO_MENSAGEM);
      this.jogosModel = new JogosModel();
    });
  }

  prepararModel() {
    const control = this.jogosForm.controls;
    this.jogosModel.nome = control.nome.value;
    this.jogosModel.estiloJogo = control.estiloJogo.value;
    this.jogosModel.observacao = control.observacao.value;

  }
  validacao(): boolean {
    this.existeErro = false;
    const controls = this.jogosForm.controls;

    if (this.jogosForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAllAsTouched()
      );

      this.existeErro = true;
      return false;
    }
    return true;
  }

  voltarPagina() {
    this.route.navigate(['jogos/lista']);
  }
}
