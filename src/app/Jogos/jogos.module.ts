import { MatIconModule } from '@angular/material/icon';
import { JogosService } from './../core/services/jogos.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { title } from 'process';
import { Routes, RouterModule } from '@angular/router';
import { JogosCadastroComponent } from './jogos-cadastro/jogos-cadastro.component';
import { JogosListaComponent } from './jogos-lista/jogos-lista.component';
import { NgModule } from "@angular/core";
import { BoxModule } from 'angular-admin-lte';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ComponentsModule } from '../components/components.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { AlertModule as MkAlertModule } from 'angular-admin-lte';
import { MatSelectModule } from '@angular/material/select';
import { AuthInterceptor } from '../core/auth/auth.interceptor';

const route: Routes = [
    {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
    },
    {
        path: 'lista',
        component: JogosListaComponent,
        data: {
            title: 'Listagem de jogos'
        }
    },
    {
        path: 'cadastro',
        component: JogosCadastroComponent,
        data: {
            title: 'Cadastros de jogos'
        }
    },
    {
        path: 'cadastro/:id',
        component: JogosCadastroComponent,
        data: {
            title: 'Editar jogo'
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatSelectModule,
        RouterModule.forChild(route),
        BoxModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        ComponentsModule,
        MatInputModule,
        MatCheckboxModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MkAlertModule

    ], declarations: [
        JogosListaComponent,
        JogosCadastroComponent
    ],
    providers:[
        JogosService,
        {
            provide:HTTP_INTERCEPTORS,
            useClass:AuthInterceptor,
            multi:true
        }
    ]
})

export class JogosModule { }