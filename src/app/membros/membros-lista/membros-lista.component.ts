import { Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { MembroService } from './../../core/services/membro.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { DataTableColunas } from '../../components/_models/DataTableColunas';
import { DataTableAcoes } from '../../components/_models/DataTableAcoes';
import { Router } from '@angular/router';
import { MembroModel } from 'src/app/core/models/membro.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-membros-lista',
  templateUrl: './membros-lista.component.html',
  styleUrls: ['./membros-lista.component.css'],
  providers: [MembroService]
})
export class MembrosListaComponent implements OnInit {
  colunas: DataTableColunas[] = [
    { propriedade: 'id', titulo: 'Id', disabled: false, cell: (row: MembroModel) => `${row.id}` },
    { propriedade: 'name', titulo: 'Nome', disabled: false, cell: (row: MembroModel) => `${row.name}` },
    { propriedade: 'userName', titulo: 'Login', disabled: false, cell: (row: MembroModel) => `${row.username}` },
    { propriedade: 'level', titulo: 'Level', disabled: false, cell: (row: MembroModel) => `${row.descLevel}` },
    { propriedade: 'escudo', titulo: 'Escudo', disabled: false, cell: (row: MembroModel) => `${row.descShield}` },
    { propriedade: 'email', titulo: 'E-mail', disabled: false, cell: (row: MembroModel) => `${row.email}` },
    { propriedade: 'genderDescription', titulo: 'Sexo', disabled: false, cell: (row: MembroModel) => `${row.descGender}` },
    { propriedade: 'Active', titulo: 'Status', disabled: false, cell: (row: MembroModel) => `${row.active ? 'Ativo' : 'Inativo'}` }
  ];

  acoes: DataTableAcoes[] = [
    { icone: 'create', evento: this.editar.bind(this), toolTip: 'Editar', color: 'primary' },
    { icone: 'delete', evento: this.deletar.bind(this), toolTip: 'Deletar', color: 'warn' }
  ];

  dadosTabelaCopia: MembroModel[] = [];
  dadosTabela: MembroModel[] = [];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  filtroNome: string = '';
  /**
   *
   */
  constructor(
    private membroService: MembroService,
    private route: Router,
    private toastService: ToastrService
  ) {

  }

  ngOnInit() {
    this.membroService.obterTodos().subscribe((result: any) => {
      this.dadosTabela = result.lista;
      //cópia para fazer filtros.
      this.dadosTabelaCopia = result.lista;
    });

  }

  adicionar() {
    this.route.navigate(['membros/cadastro/add']);
  }
  editar(membro: MembroModel) {
    this.route.navigate([`membros/cadastro/edit/${membro.id}`]);
  }

  deletar(membroModel: MembroModel) {

    const mensagem = !membroModel.active ? 'Gostaria de ativar esse membro?' : 'Gostaria de inativar esse membro?';
    Swal.fire({
      title: 'Inativar/Ativar membro',
      text: mensagem,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Excluir',
      cancelButtonText: 'Cancelar'
    }).then((result: any) => {
      if (result.isConfirmed) {
        membroModel.active = false;
        this.membroService.ativar(membroModel.id, membroModel.active, membroModel.observation).subscribe((result: any) => {
          if (result.dados)
            this.toastService.success("Operação realizada com sucesso", "Membros");
          else
            this.toastService.warning("Erro ao realizar essa operação", "Membros");
        });
      }
    });

  }

  buscar() {
    if (this.filtroNome === '') {
      this.dadosTabela = this.dadosTabelaCopia;
      return;
    }

    const buscaDados = this.dadosTabela.filter(x => x.id.toString().includes(this.filtroNome) || x.name.toUpperCase().includes(this.filtroNome));
    this.dadosTabela = buscaDados;
  }

}


