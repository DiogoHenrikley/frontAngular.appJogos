import { Util } from './../../core/util/util';
import { environment } from './../../../environments/environment';
import { EscudoService } from './../../core/services/escudo.service';
import { UploadModel } from './../../core/models/upload.model';
import { EstadoService } from './../../core/services/estado.service';
import { CidadeService } from './../../core/services/cidade.service';
import { ToastrService } from 'ngx-toastr';
import { MembroService } from './../../core/services/membro.service';
import { MembroModel } from 'src/app/core/models/membro.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UploadService } from 'src/app/core/services/upload.service';
import { enUploadPasta } from 'src/app/core/enumeradores/enuploadPasta';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-membros-cadastro',
  templateUrl: './membros-cadastro.component.html',
  styleUrls: ['./membros-cadastro.component.css'],
  providers: [MembroService, CidadeService, EstadoService, UploadService,
    EscudoService]
})
export class MembrosCadastroComponent implements OnInit {

  filtroNome: string = '';
  tituloPagina: string = 'Cadastro de Membros';
  membroModel = new MembroModel();
  file: File;
  hide = true;
  carregouImagem = false;
  frmmembro: FormGroup;
  mostrarMensagemSenha = false;
  listaLevel = [
    { id: 1, description: 'Usuário' },
    { id: 2, description: 'Administrador' }
  ];

  tipoContaJogo = [
    { id: 1, description: 'Steam' },
    { id: 2, description: 'Xbox' },
    { id: 3, description: 'PSN' }
  ];

  cidades = [];
  estados = [];

  divisaoJogador = [
    { id: 1, description: 'Primeira' },
    { id: 1, description: 'Segunda' },
    { id: 1, description: 'Terceira' },
    { id: 1, description: 'Quarta' }
  ];

  sexo = [
    { id: 0, description: 'F' },
    { id: 1, description: 'M' }
  ];

  statusInscricao = [
    { id: 1, description: 'Confirmado' },
    { id: 2, description: 'Não Confirmado' },
    { id: 3, description: 'Fora do Periodo' },
    { id: 4, description: 'Não pagou' }
  ];
  escudos = [];
  urlImagemMembro = '';
  base64Image = '';
  nomeImagemPrevisualizacao = '';
  existeErroForm = false;

  constructor(private route: Router,
    private fbMembro: FormBuilder,
    private membroService: MembroService,
    private cidadeService: CidadeService,
    private estadoService: EstadoService,
    private uploadService: UploadService,
    private escudoService: EscudoService,
    private toast: ToastrService,
    private activedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.loadMembro();
    this.obterEstados();
    this.obterEscudo();

    this.activedRoute.params.subscribe((param: any) => {
      const id = param.id;
      if (id !== undefined && id > 0) {
        this.tituloPagina = `Editar membro N° ${id}`;
        this.membroService.obterPorId(id).subscribe((result: any) => {
          this.membroModel = result.dados;
          this.setarDadosCidadeEstado(this.membroModel.estadoId, this.membroModel.cidadeId);
          this.loadMembro();
          this.montarImagemMembro();
        });
      }

    });

  }

  montarImagemMembro() {
    if (this.membroModel && this.membroModel.image !== '')
      this.urlImagemMembro = `${environment.url_image}Membros/${this.membroModel.image}`;
    else
      this.urlImagemMembro = '';
  }

  /**
   * Método para setar a cidade cadastrada do usuário.
   * @param estadoId Código do estado
   * @param cidadeId Código da cidade para setar no controle de cidade.
   */
  setarDadosCidadeEstado(estadoId: number, cidadeId: number) {
    this.cidadeService.obterCidadePorIdEstado(estadoId).subscribe((result: any) => {
      this.cidades = result.lista;
      this.frmmembro.controls["cidadeId"].setValue(this.membroModel.cidadeId);
    });
  }


  loadMembro() {
    this.frmmembro = this.fbMembro.group({
      name: [this.membroModel.name, Validators.required],
      email: [this.membroModel.email, [Validators.required, Validators.email]],
      mobile: [this.membroModel.mobile, [Validators.required, Validators.maxLength(11)]],
      gender: [this.membroModel.gender, Validators.required],
      active: [this.membroModel.active, Validators.required],
      estadoId: [this.membroModel.estadoId, Validators.required],
      cidadeId: [this.membroModel.clienteId, Validators.required],
      levelId: [this.membroModel.levelId, Validators.required],
      file: ['file'],
      username: [this.membroModel.username, [Validators.required, Validators.maxLength(100)]],
      password: [this.membroModel.password, [Validators.required, Validators.maxLength(10)]],
      confirmPassword: [this.membroModel.password, [Validators.required, Validators.maxLength(10)]],
      account: [this.membroModel.account,Validators.required],
      sponsorship: [this.membroModel.sponsorship],
      subscription: [this.membroModel.subscription],
      escudoId: [this.membroModel.escudoId,Validators.required],
      division: [this.membroModel.division,Validators.required],
      dateOfBirth: [this.membroModel.dateOfBirth ? new Date(this.membroModel.dateOfBirth) : "",Validators.required],
      proNegotiate: [this.membroModel.proNegotiate],
      proSteals: [this.membroModel.proSteals],
      proUserStealing: [this.membroModel.proUserStealing],
      grades: [this.membroModel.grades],
      observation: [this.membroModel.observation],
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.frmmembro.controls[controlName];
    if (!control) {
      return false;
    }

    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  /**
   * Método para setar o arquivo no modelo.
   * @param event Evento disparado do upload.
   */
  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      this.file = event.target.files;
      this.membroModel.carregouImagem = true;
      this.previewImagem(this.file);
    }
  }

  /**
   * Método para carregar o arquivo de upload.
   * @param file Arquivo que será carregado.
   */
  previewImagem(file: File) {
    if (!file && file.size === 0) return;
    const fileReader = new FileReader();
    fileReader.onloadend = () => {
      this.base64Image = fileReader.result.toString();
      this.nomeImagemPrevisualizacao = file[0].name;
    };
    fileReader.readAsDataURL(file[0]);
  }

  /**
   * Método para carregar as cidades com base no estado passado.
   * @param dados código do estado para carregar as cidades.
   */
  obterCidadePorIdEstado(dados) {
    this.cidadeService.obterCidadePorIdEstado(dados.value).subscribe((result: any) => {
      this.cidades = result.lista;
    });
  }
  obterEscudo() {
    this.escudoService.obterEscudos().subscribe((result: any) => {
      this.escudos = result.dados;
    });
  }
  obterEstados() {
    this.estadoService.obterEstados().subscribe((result: any) => {
      this.estados = result.lista;
    });
  }
  /**
   * Validar form antes de salvar
   */
  validarForm() {
    const control = this.frmmembro.controls;
    if (this.frmmembro.invalid) {
      Object.keys(control).forEach(name => {
        control[name].markAllAsTouched();
      });
      this.existeErroForm = true;
      return false;
    }
    this.existeErroForm = false;
    return true;

  }

  /**
   * salvar dados
   */
  salvar() {
    this.prepararDadosModel();
    if (!this.validarForm()) return;
    this.membroService.controladorOperacaoSalvarAtualizar(this.membroModel).subscribe((result: any) => {
      if (result.success) {
        this.toast.success("Operação realizada com sucesso", "Membro")
        this.membroModel.id = result.dados;
        if (this.file) {
          this.uploadImagem().subscribe((result: any) => {
            if (result.success)
              this.toast.success("Upload realizado com sucesso.", "Upload");
            else
              this.toast.success("Erro ao realizar o upload.", "Upload");
          });
        }
      }
      else
        this.toast.error("Erro ao realizar essa operação", "Membro");
    });
  }

  /**
   * Método para fazer upload da imagem.
   * @author Diogo Martins
   * @date  01/09/2020
   */
  uploadImagem() {
    // id_membro + id_cliente
    const fileName = this.montarNomeImageUpload();
    this.membroModel.urlImagem = fileName.trim();
    this.membroModel.image = fileName.trim();
    let upload = new UploadModel();
    upload.id = this.membroModel.id;
    upload.nomeArquivo = this.montarNomeImageUpload();
    upload.uploadPasta = enUploadPasta.Membros;
    return this.uploadService.upload(this.file, upload);

  }

  voltarPagina() {
    this.route.navigate(['membros/lista']);
  }

  deletar() {
    this.activedRoute.params.subscribe((param: any) => {
      const id = param.id;
      this.membroService.ativar(this.membroModel.id, this.membroModel.active, this.membroModel.observation).subscribe((result: any) => {
        if (result.dados)
          this.toast.success("Operação realizada com sucesso", "Membros");
        else
          this.toast.warning("Erro ao realizar essa operação", "Membros");
      });
    });
  }

  preencheDadosMok() {
    this.membroModel.name = "Diogo Henrikley F Martins";
    this.membroModel.email = "diogohenrikey@gmail.com";
    this.membroModel.mobile = "62-985397617";
    this.membroModel.gender = 1;
    this.membroModel.active = true;
    this.membroModel.estadoId = 9;
    this.membroModel.cidadeId = 901;
    this.membroModel.urlImagem = "imagemteste";
    this.membroModel.image = "";
    this.membroModel.levelId = 1;
    this.membroModel.clienteId = 1;
    this.membroModel.username = "diogo.martins";
    this.membroModel.password = "123456";
    this.membroModel.confirmPassword = this.membroModel.password;
    this.membroModel.account = 1;
    this.membroModel.sponsorship = 10120;
    this.membroModel.subscription = 1;
    this.membroModel.escudoId = 1;
    this.membroModel.division = 1;
    this.membroModel.dateOfBirth = new Date();
    this.membroModel.proNegotiate = true;
    this.membroModel.proSteals = true;
    this.membroModel.proUserStealing = true;
    this.membroModel.grades = "Teste de insert mock";
    this.membroModel.observation = "teste de observação";

  }

  montarNomeImageUpload() {
    if (this.file) {
      return ` ${this.membroModel.id}-${this.membroModel.clienteId}.${this.file[0].name.split('.')[1]}`;
    }
    return '';
  }

  prepararDadosModel(): MembroModel {
    const control = this.frmmembro.controls;
    // Object.entries(this.membroModel).forEach(entidade=>{
    //   Object.entries(this.frmmembro.controls).forEach(controlName=>{
    //     if(entidade[0] === controlName.toString()){
    //       entidade.values = controlName.values;
    //     }
    //   })
    // })
    //this.membroModel.id = control.id.value;
    this.membroModel.clienteId = Util.obterIdCliente();
    this.membroModel.name = control.name.value;
    this.membroModel.email = control.email.value;
    this.membroModel.mobile = control.mobile.value;
    this.membroModel.gender = control.gender.value;
    this.membroModel.active = control.active.value;
    this.membroModel.estadoId = control.estadoId.value;
    this.membroModel.cidadeId = control.cidadeId.value;
    this.membroModel.levelId = control.levelId.value;
    this.membroModel.username = control.username.value;
    this.membroModel.password = control.password.value;
    this.membroModel.account = control.account.value;
    this.membroModel.sponsorship = control.sponsorship.value;
    this.membroModel.subscription = control.subscription.value;
    this.membroModel.escudoId = control.escudoId.value;
    this.membroModel.division = control.division.value;
    this.membroModel.dateOfBirth = control.dateOfBirth.value;
    this.membroModel.proNegotiate = control.proNegotiate.value;
    this.membroModel.proSteals = control.proSteals.value;
    this.membroModel.proUserStealing = control.proUserStealing.value;
    this.membroModel.grades = control.grades.value;
    this.membroModel.grades = control.observation.value;

    return this.membroModel;
  }

  /**
   * Validar se as senhas são iguais.
   */
  validarSenha() {
    const confirmSenha = this.frmmembro.controls;

    if (confirmSenha.confirmPassword.value !== this.membroModel.password)
      this.mostrarMensagemSenha = true;
    else {
      this.mostrarMensagemSenha = false;
    }
  }
}
