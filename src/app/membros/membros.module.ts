import { AppDateAdapter, APP_DATE_FORMATS } from './../core/config/format-datepicker';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MembrosComponent } from './membros.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MembrosListaComponent } from './membros-lista/membros-lista.component';
import { MembrosCadastroComponent } from './membros-cadastro/membros-cadastro.component';
import { BoxModule } from 'library/angular-admin-lte/src/lib/box/box.module';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { GridViewComponent } from '../components/grid-view/grid-view.component';
import { AppModule } from '../app.module';
import { ComponentsModule } from '../components/components.module';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { AlertModule as MkAlertModule } from 'angular-admin-lte';
import { MatCardModule } from '@angular/material/card'
import { MatDividerModule } from '@angular/material/divider';
import Swal from 'sweetalert2';
import { AuthInterceptor } from '../core/auth/auth.interceptor';
import { LoaderInterceptorService } from '../core/layout/loader/loader.interceptor';

const routes: Routes = [
  {
    path: '',
    component: MembrosComponent,
    children: [
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      },
      {
        path: 'lista',
        component: MembrosListaComponent,
        data: {
          title: 'Lista de Membros'
        }
      },
      {
        path: 'cadastro',
        component: MembrosCadastroComponent,
        data: {
          title: 'Cadastro de Membros'
        }
      },
      {
        path: 'cadastro/edit/:id',
        component: MembrosCadastroComponent,
        data: {
          title: 'Editar Membro'
        }
      },
      {
        path: 'cadastro/add',
        component: MembrosCadastroComponent,
        data: {
          title: 'Cadastro Membro'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    BoxModule,
    MatInputModule,
    MatButtonModule,
    MatTabsModule,
    MatSelectModule,
    MatIconModule,
    ComponentsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MkAlertModule,
    MatCardModule,
    MatDividerModule

  ],
  declarations: [
    MembrosComponent,
    MembrosCadastroComponent,
    MembrosListaComponent
  ],
  providers: [
    MatDatepickerModule,
    MatNativeDateModule,
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'pt-PT' },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    }
  ]

})
export class MembrosModule { }
