import { Util } from './../core/util/util';
import { MembroService } from './../core/services/membro.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../core/services/login.service';
import { tap } from 'rxjs/internal/operators/tap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private toast: ToastrService
    , private fb: FormBuilder
    , private loginService: LoginService
    , private membroService: MembroService
    , private router: Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['diogohenrikley@gmail.com', [Validators.required, Validators.email]],
      password: ['123456', [Validators.required]]
    });
  }

  onLogar() {

    this.loginService.autenticar(this.loginForm.value)
      .subscribe((login: any) => {
        if (login.success) {
          debugger;
          this.toast.success('Login realizado com sucesso!');
          localStorage.setItem('token', login.dados.token);
          localStorage.setItem('user', btoa(JSON.stringify(login.dados)));
          this.router.navigateByUrl('/home');
        } else {
          this.toast.error('Login/Senha está incorreto!');
        }
      });




    // this.loginService.login(this.loginForm.value)
    // .pipe(
    //   tap(
    //     user => {
    //       if (user){
    //         this.toast.success('Login realizado com sucesso!');
    //         localStorage.setItem('token', user.token);
    //         this.router.navigateByUrl('/'); 
    //       }
    //     }
    //   )
    // )
    // .subscribe(
    //   (data) => {console.log(data);} 
    // );

  }

}
