import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { DataTableColunas } from '../_models/DataTableColunas';
import { DataTableAcoes } from '../_models/DataTableAcoes';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-grid-view',
  templateUrl: './grid-view.component.html',
  styleUrls: ['./grid-view.component.css']
})
export class GridViewComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  @Input() colunas: DataTableColunas[] = [];
  @Input() acoes: DataTableAcoes[] = [];
  @Input() set dadosTabela(itens: any[]) {
    this.dataSource.data = itens;
  }
  @Input("usarIMG")usarIMG:boolean =false;

  dataSource: any = new MatTableDataSource<any>();
  displayedColumns = [];

  constructor() { }

  ngOnInit() {
    this.colunas.forEach(x => {
      this.displayedColumns.push(x.propriedade)
    })
  
    this.displayedColumns.push('acoesheader');
    this.dataSource.paginator = this.paginator;
  }

}
