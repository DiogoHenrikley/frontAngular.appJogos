import { ToastrService } from 'ngx-toastr';
import { GerenciarEmprestimoService } from './../../core/services/gerenciar-emprestimo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AmigosService } from 'src/app/core/services/amigos.service';
import { JogosService } from './../../core/services/jogos.service';
import { EmprestimoModel } from './../../core/models/emprestimo.mode';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-emprestimo-cadastro',
  templateUrl: './emprestimo-cadastro.component.html',
  styleUrls: ['./emprestimo-cadastro.component.css']
})
export class EmprestimoCadastroComponent implements OnInit {

  existeErro = false;
  empForm:FormGroup;
  jogos = [];
  amigos = [];
  emprestimoModel = new EmprestimoModel();
  constructor(private fb:FormBuilder,
    private jogosService:JogosService,
    private amigosService:AmigosService,
    private router:Router,
    private activeRouter:ActivatedRoute,
    private gerenciarEmprestimo:GerenciarEmprestimoService,
    private toastrService:ToastrService) { }

  ngOnInit(): void {

    this.activeRouter.params.subscribe((result:any)=>{
      const id= result.id;
      if(id !== undefined && id > 0){
        this.obterPorId(id);
      }
    });

    this.loadEmprestimo(new EmprestimoModel());
    this.obterTodosAmigos();
    this.obterTodosJogos();
  }

  obterPorId(id:number){
    this.gerenciarEmprestimo.obterPorId(id).subscribe((result:any)=>{
      this.loadEmprestimo(result.dados);
    });
  }

  obterTodosAmigos(){
    this.amigosService.obterTodos().subscribe((result:any)=>{
      this.amigos = result.dados;
    });
  }

  obterTodosJogos(){
    this.jogosService.obterTodos().subscribe((result:any)=>{
      this.jogos = result.dados ;
    });

  }

  loadEmprestimo(_emprestimoModel:EmprestimoModel){
    this.emprestimoModel = _emprestimoModel;
    
    this.empForm = this.fb.group({
      amigoId:[this.emprestimoModel.amigoId,Validators.required],
      jogoId:[this.emprestimoModel.jogoId,Validators.required]
    });

  }
  salvar(){
    if(!this.validacao()) return;
    this.prepararModel();
    this.gerenciarEmprestimo.gerenciarInsertUpdate(this.emprestimoModel).subscribe((result:any)=>{
      if(result.success){
        this.toastrService.success('Operação realizada com sucesso.','Emprestimo de jogos');
        this.loadEmprestimo(new EmprestimoModel());
      }else{
        debugger;
        const erro = result.dados;
        const mens = 'Erro ao realizar essa operação';
        this.toastrService.error(erro === undefined ? mens : erro,'Empréstimo de jogos');
      }
    });
  }

  prepararModel(){
    const control = this.empForm.controls;
    this.emprestimoModel.jogoId = control.jogoId.value;
    this.emprestimoModel.amigoId = control.amigoId.value;
    return this.emprestimoModel;
  }

  validacao(): boolean {
    this.existeErro = false;
    const controls = this.empForm.controls;

    if (this.empForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAllAsTouched()
      );
      this.existeErro = true;
      return false;
    }

    return true;
  }

  voltarPagina(){
    this.router.navigate(['emprestimo/lista']);
  }

}
