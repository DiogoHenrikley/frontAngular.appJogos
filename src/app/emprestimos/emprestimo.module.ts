import { AmigosService } from 'src/app/core/services/amigos.service';
import { JogosService } from './../core/services/jogos.service';
import { MatIconModule } from '@angular/material/icon';
import { EmprestimosService } from './../core/services/emprestimos.service';
import { EmprestimoListaComponent } from '../emprestimos/emprestimo-lista/emprestimo-lista.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { NgModule } from "@angular/core";
import { BoxModule } from 'angular-admin-lte';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ComponentsModule } from '../components/components.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { AlertModule as MkAlertModule } from 'angular-admin-lte';
import { EmprestimoCadastroComponent } from './emprestimo-cadastro/emprestimo-cadastro.component';
import { MatSelectModule } from '@angular/material/select';
import { AuthInterceptor } from '../core/auth/auth.interceptor';

const route: Routes = [{
    path: '',
    redirectTo: 'lista',
    pathMatch: 'full'
},
{
    path: 'lista',
    component: EmprestimoListaComponent,
    data: {
        title: 'Controle de jogos'
    }
},
{
    path: 'cadastro',
    component: EmprestimoCadastroComponent,
    data: {
        title: 'Cadastro de emprestimo'
    }
},
{
    path: 'cadastro/:id',
    component: EmprestimoCadastroComponent,
    data: {
        title: 'Atualizar emprestimo'
    }
}
];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        MatSelectModule,
        RouterModule.forChild(route),
        BoxModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        ComponentsModule,
        MatInputModule,
        MatCheckboxModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MkAlertModule
    ],
    declarations: [
        EmprestimoListaComponent,
        EmprestimoCadastroComponent
    ],
    providers: [
        EmprestimosService,
        JogosService,
        AmigosService,
        {
            provide:HTTP_INTERCEPTORS,
            useClass:AuthInterceptor,
            multi:true
        }
    ]
})
export class EmprestimoModule { }