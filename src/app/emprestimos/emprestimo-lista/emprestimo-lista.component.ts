import { TraduzirEstiloJogo } from './../../core/enumeradores/enEstiloJogador';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { GerenciarEmprestimoService } from './../../core/services/gerenciar-emprestimo.service';
import { EmprestimoModel } from './../../core/models/emprestimo.mode';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { DataTableAcoes } from 'src/app/components/_models/DataTableAcoes';
import { DataTableColunas } from 'src/app/components/_models/DataTableColunas';

@Component({
  selector: 'app-emprestimo-lista',
  templateUrl: './emprestimo-lista.component.html',
  styleUrls: ['./emprestimo-lista.component.css']
})
export class EmprestimoListaComponent implements OnInit {

  filtroNome: '';
  colunas: DataTableColunas[] = [
    { propriedade: 'id', titulo: 'Id', disabled: false, cell: (row: any) => `${row.id}` },
    { propriedade: 'nomeAmigo', titulo: 'Nome', disabled: false, cell: (row: any) => `${row.nomeAmigo}` },
    { propriedade: 'nomeJogo', titulo: 'Jogo', disabled: false, cell: (row: any) => `${row.nomeJogo}` },
    { propriedade: 'estiloJogo', titulo: 'Estilo', disabled: false, cell: (row: any) => `${TraduzirEstiloJogo(row.estiloJogo)}` },
    { propriedade: 'statusJogo ', titulo: 'Status', disabled: false, cell: (row: any) => row.statusJogo },
  ];

  acoes: DataTableAcoes[] = [
    { icone: 'create', evento: this.editar.bind(this), toolTip: 'Editar', color: 'primary' },
    { icone: 'delete', evento: this.deletar.bind(this), toolTip: 'Deletar', color: 'warn' }
  ];

  dadosTabela: any[] = [];

  constructor(private router: Router,
    private gerenciarService: GerenciarEmprestimoService,
    private toastService: ToastrService) { }

  ngOnInit(): void {

      this.buscar();

  }

  editar(emprestimo: EmprestimoModel) {
    this.router.navigate([`emprestimo/cadastro/${emprestimo.id}`]);
  }

  deletar(emprestimoModel: EmprestimoModel) {
    if (emprestimoModel.id !== undefined && emprestimoModel.id > 0) {
      Swal.fire({
        cancelButtonText: 'Cancelar',
        showCancelButton: true,
        confirmButtonText: 'OK',
        text: `Gostaria de excluiro  empréstimo ${emprestimoModel.id} do amigo ${emprestimoModel.nomeAmigo}`,
        titleText: 'Excluir Emprestimo'
      }).then((result: any) => {
        if (result.isConfirmed) {
          this.gerenciarService.remover(emprestimoModel.id).subscribe((result: any) => {
            if (result.success) {
              this.toastService.success('Operação realizada com sucesso', 'Excluir Empréstimo');
              this.removerEmprestimoPorIndex(emprestimoModel);
            } else
              this.toastService.error('Erro ao realizar essa operação', 'Excluir Empréstimo');
          });
        }
      });
    }

  }

  removerEmprestimoPorIndex(emprestimoModel: EmprestimoModel) {
    this.dadosTabela = this.dadosTabela.filter(b => b.id !== emprestimoModel.id);
  }

  adicionar() {
    this.router.navigate(['emprestimo/cadastro']);
  }

  buscar() {
    const valor = Number.parseInt(this.filtroNome);
    if (valor) {
      this.gerenciarService.obterPorId(valor).subscribe((result: any) => {
        this.dadosTabela = result.dados;
        return;
      });
    } else if (this.filtroNome !== '') {
      this.gerenciarService.obterPorDescricao(this.filtroNome).subscribe((result: any) => {
        this.dadosTabela = result.dados;
        return;
      });
    }
    this.gerenciarService.obterTodos().subscribe((result: any) => {
      this.dadosTabela = result.dados;
    });

  }




}
