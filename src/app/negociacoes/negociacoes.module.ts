import { ContratacaoJogadorService } from './../core/services/contratacao-jogador.service';
import { EscudoService } from './../core/services/escudo.service';
import { JogadorService } from './../core/services/jogador.service';
import { MembroService } from './../core/services/membro.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NegociacoesListaComponent } from './negociacoes-lista/negociacoes-lista.component';
import { NegociacoesCadastroComponent } from './negociacoes-cadastro/negociacoes-cadastro.component';
import { NegociacoesComponent } from './negociacoes.components';

import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoxModule } from 'library/angular-admin-lte/src/lib/box/box.module';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { ComponentsModule } from '../components/components.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { AlertModule as MkAlertModule } from 'angular-admin-lte';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';

const route: Routes = [
  {
    path: '',
    component: NegociacoesComponent,
    children: [
      {
        path: '',
        redirectTo: 'lista',
        pathMatch: 'full'
      },
      {
        path: 'lista',
        component: NegociacoesListaComponent,
        data: {
          title:'Listagem de negociações'
        }
      },
      {
        path:'cadastro',
        component: NegociacoesListaComponent,
        data:{
          title:'Cadastro de negociações'
        }
      },
      {
         path:'cadastro/add',
         component:NegociacoesCadastroComponent,
         data:{
           title:'Cadastro de negociações'
         }
      },
      {
        path:'cadastro/edit/:id',
        component:NegociacoesCadastroComponent,
        data:{
          title:'Editar negociação N° '
        }
      }
    ]
  }

];

@NgModule({
  declarations: [
   NegociacoesCadastroComponent,
   NegociacoesComponent,
   NegociacoesListaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forChild(route),
    BoxModule,
    MatInputModule,
    MatButtonModule,
    MatTabsModule,
    MatSelectModule,
    MatIconModule,
    ComponentsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MkAlertModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatDialogModule
  ],
  providers:[
    MembroService,JogadorService,EscudoService,ContratacaoJogadorService

  ]
})
export class NegociacoesModule { }
