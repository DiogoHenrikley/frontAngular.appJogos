import { ToastrService } from 'ngx-toastr';
import { environment } from './../../../environments/environment';
import { FiltroNegociacoes } from './../../core/filtros/filtro.negociacoes';
import { enTipoContratacao } from './../../core/enumeradores/enTipoContratacao';
import { NegociacoesCadastroComponent } from './../negociacoes-cadastro/negociacoes-cadastro.component';
import { EscudoService } from './../../core/services/escudo.service';
import { JogadorModel } from './../../core/models/jogador.model';
import { JogadorService } from './../../core/services/jogador.service';
import { Util } from './../../core/util/util';
import { MembroService } from './../../core/services/membro.service';
import { DataTableAcoes } from './../../components/_models/DataTableAcoes';
import { DataTableColunas } from './../../components/_models/DataTableColunas';
import { PesquisaGenericaModel } from './../../core/models/pesquisaGenerica.model';
import { NegociacoesFiltro } from './../../core/models/negociacaoFiltro.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-negociacoes-lista',
  templateUrl: './negociacoes-lista.component.html',
  styleUrls: ['./negociacoes-lista.component.css']
})
export class NegociacoesListaComponent implements OnInit {

  panelOpenState = false;
  arrayOverall = [];
  negociacaoModel = new NegociacoesFiltro();
  filtroNegociacao = new FiltroNegociacoes();

  timeLiga: PesquisaGenericaModel[] = [];
  timeJogo: PesquisaGenericaModel[] = [];
  TITULO_MENSAGEM = "Negociação de Jogador";
  nomeHabilidade ='';

  listaHabilidades: PesquisaGenericaModel[] = [
    { id: 0, name: '' },
    { id: 1, name: 'Talento Ofensiva' },
    { id: 2, name: 'Controle de bola' },
    { id: 3, name: 'Drible' },
    { id: 4, name: 'Passe rasteiro' },
    { id: 5, name: 'Passe pelo alto' },
    { id: 6, name: 'Finalização' },
    { id: 7, name: 'Chute colocado' },
    { id: 8, name: 'Giro controlado' },
    { id: 9, name: 'Cabeçada' },
    { id: 10, name: 'Talento defensiva' },
    { id: 11, name: 'Desarme' },
    { id: 12, name: 'Força do chute' },
    { id: 13, name: 'Velocidade' },
    { id: 14, name: 'Explosão' },
    { id: 15, name: 'Equilíbrio' },
    { id: 16, name: 'Contato Físico' },
    { id: 17, name: 'Impulsão' },
    { id: 18, name: 'Resistência' },
    { id: 19, name: 'Talento como goleiro' },
    { id: 20, name: 'Catching' },
    { id: 21, name: 'Clearing' },
    { id: 22, name: 'Reflexes' },
    { id: 23, name: 'Coverage' },
    { id: 24, name: 'Condição física' },
    { id: 25, name: 'Resistência a lesão' },
    { id: 26, name: 'Pior pé (frequência)' },
    { id: 27, name: 'Pior pé (precisão)' },
    { id: 28, name: 'Agressividade' },
    { id: 29, name: 'Condução firme' }
  ];

  listaPosicoes: PesquisaGenericaModel[] = [
    { id: 1, name: 'GO' },
    { id: 1, name: 'LB' },
    { id: 1, name: 'ZC' },
    { id: 1, name: 'LE' },
    { id: 1, name: 'LD' },
    { id: 1, name: 'VOL' },
    { id: 1, name: 'MLG' },
    { id: 1, name: 'MLE' },
    { id: 1, name: 'MLD' },
    { id: 1, name: 'MAT' },
    { id: 1, name: 'PTE' },
    { id: 1, name: 'PTD' },
    { id: 1, name: 'SA' },
    { id: 1, name: 'CA' }
  ];

  lista99: number[] = [];

  colunas: DataTableColunas[] = [
    { propriedade: 'urlImage', titulo: '#', disabled: false, cell: (row: any) => `${row.urlImage === null ? environment.jogadorSemFoto : environment.url_image.concat('Jogadores/', row.urlImage)}`, usarImg: true },
    { propriedade: 'nome', titulo: 'Nome', disabled: false, cell: (row: any) => `${row.nome}` },
    { propriedade: 'overall', titulo: 'Overall', disabled: false, cell: (row: any) => `${row.overall}` },
    { propriedade: 'nacional', titulo: 'Nacionalidade', disabled: false, cell: (row: any) => `${row.nacional}` },
    { propriedade: 'posicao', titulo: 'Posição', disabled: false, cell: (row: any) => `${row.posicao}` },
    { propriedade: 'time', titulo: 'Time', disabled: false, cell: (row: any) => `${row.time}` },
    { propriedade: 'passe', titulo: 'Passe', disabled: false, cell: (row: any) => `${row.passe}` }
  ];

  acoes: DataTableAcoes[] = [
    { icone: 'card_giftcard', evento: this.abrirTelaDefinitivo.bind(this), toolTip: 'Contratar', color: 'primary' },
    { icone: 'gavel', evento: this.abrirTelaRoubar.bind(this), toolTip: 'Roubar na multa', color: 'primary' },
    { icone: 'get_app', evento: this.abrirTelaEmprestado.bind(this), toolTip: 'Pegar emprestado', color: 'primary' },
    { icone: 'attach_money', evento: this.ajustarSalario.bind(this), toolTip: 'Ajustar sallário', color: 'primary' }
  ];

  listaSimNao: PesquisaGenericaModel[] = [
    { id: -1, name: 'Todos' },
    { id: 0, name: 'Sim' },
    { id: 1, name: 'Não' }
  ]

  dadosTabela: any = [];
  tipoContrato: enTipoContratacao = enTipoContratacao.Emprestimo;

  constructor(private membroService: MembroService,
    private jogadorService: JogadorService,
    private escudoService: EscudoService,
    private dialog: MatDialog,
    private toast: ToastrService) { }

  ngOnInit(): void {
    this.motarSelectOverRall();
    this.montarLista99();
    this.obterEscudoPorMembroVinculado();
    this.obterTimesDoJogo();
    this.buscar();
  }

  /**
   * Métod para abrir a tela de popup.
   * @param entity objeto do tipo jogador model
   * @param tipoContrato tipo do contrato.
   */
  abrirTelaPopPup(entity: JogadorModel, tipoContrato: enTipoContratacao) {
    entity.tipoContrato = tipoContrato;
    const telaContratacao = this.dialog.open(NegociacoesCadastroComponent, {
      width: 'auto', height: 'auto',
      data: { jogador: entity }, hasBackdrop: false
    });
    telaContratacao.afterClosed().subscribe((result: any) => {
      console.log(result);
    })
  }

  /**
   * Método para chamar a tela de ajustar salário.
   * @param jogador Objeto do tipo Jogador que será passado para ajuste.
   */
  ajustarSalario(jogador: JogadorModel) {
    this.abrirTelaPopPup(jogador, enTipoContratacao.AjustarSalario);
  }

  /**
   * Setar o objeto que será passado para outro tela.
   * @param jogadorModel objeto do tipo jogador
   */
  abrirTelaRoubar(jogadorModel: JogadorModel) {
    this.abrirTelaPopPup(jogadorModel, enTipoContratacao.Roubar);
  }

  /**
   * Setar o objeto que será passado para outro tela.
   * @param jogadorModel  objeto que será passado.
   */
  abrirTelaEmprestado(jogadorModel: JogadorModel) {
    this.abrirTelaPopPup(jogadorModel, enTipoContratacao.Emprestimo);
  }

  /**
   * Setar o objeto que será passado para outro tela.
   * @param jogadorModel  objeto que será passado.
   */
  abrirTelaDefinitivo(jogadorModel: JogadorModel) {
    this.abrirTelaPopPup(jogadorModel, enTipoContratacao.Definitivo);
  }

  obterEscudoPorMembroVinculado() {
    this.membroService.obterMembroPorEscudoVinculado(Util.obterIdCliente()).subscribe((result: any) => {
      this.timeLiga = result.dados as PesquisaGenericaModel[];
    });
  }

  obterTimesDoJogo() {
    this.escudoService.obterEscudoAtivo().subscribe((result: any) => {
      this.timeJogo = result.dados as PesquisaGenericaModel[];
    });

  }

  buscar() {
    if (!this.validarTela()) return;
    this.jogadorService.obterJogadorPorFiltro(this.filtroNegociacao).subscribe((result: any) => {
      this.dadosTabela = result.dados as JogadorModel[];
    });

  }

  @ViewChild('matHabilidade',{static:false}) mathabilidade:MatSelect;
  /**
   * @author Diogo Henrikley
   * @function Validar filtros aplicados na tela.
   * 
   */
  validarTela() {
    if (this.filtroNegociacao.overRallFinal < this.filtroNegociacao.overRallInicial) {
      this.toast.error("O Overall final não pode ser menor do que o Overall inicial.", this.TITULO_MENSAGEM);
      return false;
    }
    if (this.filtroNegociacao.idadeFinal < this.filtroNegociacao.idadeInicial) {
      this.toast.error("A idade final não pode ser menor do que idade inicial.", this.TITULO_MENSAGEM);
      return false;
    }
    if (this.filtroNegociacao.habilidadeFinal < this.filtroNegociacao.habilidadeInicial) {
      debugger;
      this.toast.error( `O valor final do filtro ${this.nomeHabilidade} não pode ser maior do que o inicial.` , this.TITULO_MENSAGEM);
      return false;
    }
    return true;
  }

  obterTextoMatSelect(event){
   this.nomeHabilidade  = (event.source.selected._element.nativeElement).innerText.trim();
    return this.nomeHabilidade;
  }

  motarSelectOverRall() {
    const total = 120;
    for (let index = 1; index < total; index++) {
      this.arrayOverall.push(index);
    }
  }

  /**
   * Método para gerar valores de 1 até 99
   */
  montarLista99() {
    for (let index = 1; index < 100; index++) {
      this.lista99.push(index);
    }
  }

}
