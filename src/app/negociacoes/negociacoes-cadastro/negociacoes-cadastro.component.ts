import { Util } from './../../core/util/util';
import { MembroService } from './../../core/services/membro.service';
import { ContratacaoJogadorModel } from './../../core/models/contratacao.jogador.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ContratacaoJogadorService } from './../../core/services/contratacao-jogador.service';
import { enTipoContratacao, TraduzirEnTipoContrato } from './../../core/enumeradores/enTipoContratacao';
import { JogadorModel } from './../../core/models/jogador.model';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-negociacoes-cadastro',
  templateUrl: './negociacoes-cadastro.component.html',
  styleUrls: ['./negociacoes-cadastro.component.css']
})
export class NegociacoesCadastroComponent implements OnInit {

  TITULO_PAGINA = 'Contratação de Jogador';
  tituloPagina = 'Contratar jogador';
  tipoContrato = [
    { id: 1, name: 'Empréstimo' },
    { id: 2, name: 'Definitivo' },
    { id: 3, name: 'Roubo' }
  ];
  listaUsuario = [];
  
  frmNegociacao: FormGroup;
  negociacaoModel = new ContratacaoJogadorModel;
  existeErroForm = false;

  constructor(public dialogRef: MatDialogRef<NegociacoesCadastroComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private contratacaoService: ContratacaoJogadorService,
    private toastService: ToastrService,
    private fb: FormBuilder,
    private membroService: MembroService

  ) { }

  ngOnInit(): void {
    this.tituloPagina = this.obterTipoContrato(this.data.jogador);
    this.preencherObjetoContratacaoJogador(this.data.jogador);
    this.loadForm();
    this.obterMembrosPorIdCliente();
  }

  preencherObjetoContratacaoJogador(jogador: JogadorModel) {
    this.negociacaoModel.nomeJogador = jogador.nomeCompleto;
    this.negociacaoModel.jogadorId = jogador.id;
    this.negociacaoModel.status = jogador.tipoContrato;
    this.negociacaoModel.statusDescricao = TraduzirEnTipoContrato(jogador.tipoContrato);
  }

  loadForm() {
    this.frmNegociacao = this.fb.group({
      nomeJogador: [this.negociacaoModel.nomeJogador],
      valorPasse: [this.negociacaoModel.valorPasse],
      statusDescricao: [this.negociacaoModel.statusDescricao],
      valorMinimo: [this.negociacaoModel.valorMinimo],
      member: [this.negociacaoModel.membroId, Validators.required],
      valorPadrao: [this.negociacaoModel.valorPadrao, Validators.required],
      observacao: [this.negociacaoModel.observacao],
    });
  }
  obterTipoContrato(objeto: JogadorModel) {
    switch (objeto.tipoContrato) {
      case enTipoContratacao.Emprestimo:
        return `Empréstimo do jogador ${objeto.nomeCompleto} `;
      case enTipoContratacao.Definitivo:
        return `Contratação definitiva de ${objeto.nomeCompleto}`;
      case enTipoContratacao.Roubar:
        return `Roubar jogador ${objeto.nomeCompleto}`;
      default:
        break;
    }
  }

  obterMembrosPorIdCliente() {
    this.membroService.obterMembroPorEscudoVinculado(Util.obterIdCliente()).subscribe((result: any) => {
      debugger;
      this.listaUsuario = result.dados;
    });
  }



  salvar() {
    if (this.validarForm())
      this.contratacaoService.gerenciarContratacao(this.negociacaoModel).subscribe((result:any) => {
        debugger;
        if (result.success) {
          this.toastService.success('Operação realizada com sucesso.', this.TITULO_PAGINA);
          this.dialogRef.close();
        }
        else
          this.toastService.error('Erro ao realizar essa operação.', this.TITULO_PAGINA);
      });
  }

  validarForm() {
    const control = this.frmNegociacao.controls;
    if (this.frmNegociacao.invalid) {
      Object.keys(control).forEach(name => {
        control[name].markAsTouched();
      });
      this.existeErroForm = true;
      return false;
    }
    debugger;
    this.negociacaoModel.membroId = control.member.value;
    this.negociacaoModel.valorMinimo = control.valorMinimo.value;
    this.negociacaoModel.valorPadrao = control.valorPadrao.value;
    this.negociacaoModel.observacao = control.observacao.value;
    this.existeErroForm = false;
    return true;
  }

}
