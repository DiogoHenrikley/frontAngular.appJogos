import { MatSelectModule } from '@angular/material/select';
import { ToastrService } from 'ngx-toastr';
import { AmigosService } from './../core/services/amigos.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { title } from 'process';
import { AmigosCadastroComponent } from './amigos-cadastro/amigos-cadastro.component';
import { AmigosListaComponent } from './amigos-lista/amigos-lista.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from "@angular/core";
import { BoxModule } from 'angular-admin-lte';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ComponentsModule } from '../components/components.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { AlertModule as MkAlertModule } from 'angular-admin-lte';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { AmigosComponent } from './amigos.components';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AuthInterceptor } from '../core/auth/auth.interceptor';

const route: Routes = [{

    path: '',
    component: AmigosComponent,
    children: [
        {
            path: '',
            redirectTo: 'lista',
            pathMatch: 'full'
        },
        {
            path: 'lista',
            component: AmigosListaComponent,
            data: {
                title: 'Lista de amigos'
            }
        },
        {
            path: 'cadastro',
            component: AmigosCadastroComponent,
            data: {
                title: 'Cadastro de amigos'
            }
        },
        {
            path: 'cadastro/:id',
            component: AmigosCadastroComponent,
            data: {
                title: 'Editar usuário '
            }
        }

    ]
}];

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(route),
        BoxModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatButtonModule,
        MatInputModule,
        ComponentsModule,
        MatIconModule,
        MatCheckboxModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MkAlertModule,
        MatTabsModule,
        MatCardModule
    ],
    declarations: [
        AmigosComponent,
        AmigosCadastroComponent,
        AmigosListaComponent
    ],
    providers:[
        AmigosService,
        ToastrService,
        {
            provide:HTTP_INTERCEPTORS,
            useClass:AuthInterceptor,
            multi:true
        }
        
    ]

})
export class AmigoModule {}