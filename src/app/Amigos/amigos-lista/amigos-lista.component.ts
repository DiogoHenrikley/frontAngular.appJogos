import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AmigoModel } from './../../core/models/amigo.model';
import { AmigosService } from './../../core/services/amigos.service';
import { Component, OnInit } from '@angular/core';
import { DataTableAcoes } from 'src/app/components/_models/DataTableAcoes';
import { DataTableColunas } from 'src/app/components/_models/DataTableColunas';

@Component({
  selector: 'app-amigos-lista',
  templateUrl: './amigos-lista.component.html',
  styleUrls: ['./amigos-lista.component.css']
})
export class AmigosListaComponent implements OnInit {

  dadosTabela: AmigoModel[] = [];
  amigoModel = new AmigoModel();
  filtroNome = '';

  colunas: DataTableColunas[] = [
    { propriedade: 'id', titulo: 'Id', disabled: false, cell: (row: AmigoModel) => `${row.id}` },
    { propriedade: 'name', titulo: 'Nome', disabled: false, cell: (row: AmigoModel) => `${row.nome}` },
    { propriedade: 'flagActive', titulo: 'Sexo', disabled: false, cell: (row: AmigoModel) => row.descricaoSexo },
  ];

  acoes: DataTableAcoes[] = [
    { icone: 'create', evento: this.editar.bind(this), toolTip: 'Editar', color: 'primary' },
    { icone: 'delete', evento: this.remover.bind(this), toolTip: 'Deletar', color: 'warn' }
  ];

  constructor(public AmigosService: AmigosService,
    private router: Router,
    private toastService: ToastrService) { }

  ngOnInit(): void {
    this.buscar();
  }

  buscar() {

    const valor = Number.parseInt(this.filtroNome);
    if (valor) {
      this.AmigosService.obterPorId(valor).subscribe((result: any) => {
        const lista: AmigoModel[] = [];
        lista.push(result.dados);
        this.dadosTabela = lista;
        return;
      });
    } else if (this.filtroNome !== '') {
      this.AmigosService.obterPorDescricao(this.filtroNome).subscribe((result: any) => {
        this.dadosTabela = result.dados;
        return;
      });
    }

    this.AmigosService.obterTodos().subscribe((result: any) => {
      this.dadosTabela = result.dados;
    })
  }

  adicionar() {
    this.router.navigate(['amigos/cadastro']);
  }

  editar(amigosModel: AmigoModel) {
    this.router.navigate([`amigos/cadastro/${amigosModel.id}`]);
  }

  remover(amigoModel: AmigoModel) {
    if (amigoModel.id !== undefined || amigoModel.id > 0) {
      Swal.fire({
        cancelButtonText: 'Cancelar',
        showCancelButton: true,
        confirmButtonText: 'OK',
        text: `Gostaria de excluir o ${amigoModel.nome}`,
        titleText: 'Excluir Jogos'
      })
        .then((result: any) => {
          if (result.isConfirmed) {
            this.AmigosService.remover(amigoModel.id).subscribe((result: any) => {
              if (result.success) {
                this.toastService.success('Operação realizada com sucesso.', 'Excluir Amigo');
                this.removeAmigoPorIndex(amigoModel);
              } else
                this.toastService.error('Erro ao realizar essa operação', 'Excluir Amigo');
            });
          }
        });
    }
  }

  removeAmigoPorIndex(amigoModel: AmigoModel) {
    this.dadosTabela = this.dadosTabela.filter(x => x.id !== amigoModel.id);
  }

}
