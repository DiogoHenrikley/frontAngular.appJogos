import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { AmigoModel } from './../../core/models/amigo.model';
import { Component, OnInit } from '@angular/core';
import { AmigosService } from 'src/app/core/services/amigos.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-amigos-cadastro',
  templateUrl: './amigos-cadastro.component.html',
  styleUrls: ['./amigos-cadastro.component.css']
})
export class AmigosCadastroComponent implements OnInit {

  amigoForm: FormGroup;
  amigoModel = new AmigoModel();
  existeErro = false;

  TITLE_PAGE = 'Cadastro de Amigos';

  sexo = [{ id: 1, name: 'Masculino' }, { id: 2, name: 'Feminino' }];

  constructor(private amigoService: AmigosService,
    private toastService: ToastrService,
    private amigoFB: FormBuilder,
    private activator: ActivatedRoute,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.loadAmigo(this.amigoModel);
    this.activator.params.subscribe((result: any) => {
      const id = result.id;
      if (id !== undefined && id > 0) {
        this.obterPorId(id);
      }
    })
  }

  obterPorId(id: number) {
    this.amigoService.obterPorId(id).subscribe((result: any) => {
      this.loadAmigo(result.dados);
    })
  }

  validacao(): boolean {
    this.existeErro = false;
    const controls = this.amigoForm.controls;

    if (this.amigoForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAllAsTouched()
      );

      this.existeErro = true;
      return false;
    }

    return true;

  }

  loadAmigo(_amigo: AmigoModel) {

    this.amigoModel = _amigo;

    this.amigoForm = this.amigoFB.group({
      nome: [this.amigoModel.nome, Validators.required],
      sexo: [this.amigoModel.sexo, Validators.required],
    });
  }

  prepararModel() {
    const control = this.amigoForm.controls;
    this.amigoModel.nome = control.nome.value;
    this.amigoModel.sexo = control.sexo.value;
    return this.amigoModel;
  }
  salvar() {
    if (!this.validacao()) return;
    this.prepararModel();
    this.amigoService.gerenciarInsertUpdate(this.amigoModel).subscribe((result: any) => {
      if (result.success) {
        this.toastService.success('Operação realizada com sucesso', this.TITLE_PAGE);
        this.loadAmigo(new AmigoModel());
      } else
        this.toastService.error('Erro ao realizar essa operação', this.TITLE_PAGE);
    });

  }

  voltarPagina() {
    this.route.navigate(['amigos/lista']);
  }

  excluir() {
    Swal.fire('Confirma exclusão desse amigo?').then((result: any) => {
      if (result.isConfirmed) {
        if (this.amigoModel.id !== undefined && this.amigoModel.id > 0) {
          this.amigoService.remover(this.amigoModel.id).subscribe((result: any) => {
            if (result.success) {
              this.toastService.success('Operação realizada com sucesso', this.TITLE_PAGE);
            }
          });
        }
      }
    });

  }

}
