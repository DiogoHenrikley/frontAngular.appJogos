FROM  node:latest as angular


WORKDIR /app

COPY package.json  /app

RUN  npm install  --silent

COPY .  .

RUN  npm run build 


EXPOSE 4200 

FROM  nginx:alpine

VOLUME  /var/cache/nginx

COPY --from=angular app/dist/angular-admin-lte-demo /usr/share/nginx/html


